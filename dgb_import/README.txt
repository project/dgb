
DGB Import allows to import guestbooks of the Guestbook module

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

- The Drupal Guestbook (DGB) module.

Installation
--------------------------------------------------------------------------------
Copy the DGB Import module folder to your module directory and then enable on
the admin modules page.

Administration
--------------------------------------------------------------------------------
No administration available.

Usage
--------------------------------------------------------------------------------
Use the import function in admin/settings/dgb/import.

To import from Guestbook it is not required that the Guestbook module is
enabled.

Note: The import will remove all existing entries in the DGB table.

If the import with DGB Import not possible please use other possibilities. For
example you can use BigDump.

